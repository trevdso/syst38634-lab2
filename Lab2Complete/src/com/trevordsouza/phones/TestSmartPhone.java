package com.trevordsouza.phones;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestSmartPhone {

	//getFormattedPrice() tests
	@Test
	public void testGetFormattedPriceRegular() {
		String price = new SmartPhone("Samsung", 878.23, 2.3).getFormattedPrice();
		assertTrue("The price provided does not match the result", price.equals("$878.23"));
	}
	
	@Test
	public void testGetFormattedPriceException() {
		String price = new SmartPhone("Samsung", 0, 2.3).getFormattedPrice();
		assertTrue("The prive provided does not match the result", price.equals("$0"));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryIn() {
		String price = new SmartPhone("Samsung", 0.01, 2.3).getFormattedPrice();
		assertTrue("The price provided does not match the result", price.equals("$0.01"));
	}
	
	@Test
	public void testGetFormattedPriceBoundaryOut() {
		String price = new SmartPhone("Samsung", 4999.99, 2.3).getFormattedPrice();
		assertTrue("The price provided does not match the result", price.equals("$4,999.99"));
	}
	
	//setVersion() tests
	@Test 
	public void testSetVersionRegular() throws VersionNumberException {
		SmartPhone sp = new SmartPhone();
		sp.setVersion(2.3);
		assertTrue("The version provided does not match the result", sp.getVersion() == 2.3);
	}
	
	@Test (expected=VersionNumberException.class)
	public void testSetVersionException() throws VersionNumberException {
		SmartPhone sp = new SmartPhone();
		sp.setVersion(7);
		fail("Wrong version value input");
	}
	
	@Test
	public void testSetVersionBoundaryIn() throws VersionNumberException {
		SmartPhone sp = new SmartPhone();
		sp.setVersion(0);
		assertTrue("The version provided does not match the result", sp.getVersion() == 0);
	}
	
	@Test (expected=VersionNumberException.class)
	public void testSetVersionBoundaryOut() throws VersionNumberException {
		SmartPhone sp = new SmartPhone();
		sp.setVersion(4.1);
		assertTrue("The version provided does not match the result", sp.getVersion() == 4.1);
	}

}
