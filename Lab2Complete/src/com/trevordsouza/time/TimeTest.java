package com.trevordsouza.time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {
	
	@Test
	public void testGetMillisecondsRegular() {
		int totalMilliseconds = Time.getMilliseconds("12:05:05:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetMillisecondsException() {
		int totalMilliseconds = Time.getMilliseconds("12:05:05:0B");
		fail("Invalid milliseconds");
	}
	
	//assuming that ms doesnt go over 60
	@Test 
	public void testGetMillisecondsBoundaryIn() {
		int totalMilliseconds = Time.getMilliseconds("00:00:00:59");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 59);
	}
	
	//assuming that ms doesnt go over 60
	@Test (expected=NumberFormatException.class)
	public void testGetMillisecondsBoundaryOut() {
		int totalMilliseconds = Time.getMilliseconds("12:05:05:60");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 00);
	}
	
	//------

	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The time provided does not match the result", totalSeconds == 3661);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0A");
		fail("The time provided is not valid");
	}
	
	@Test
	public void testGetTotalSecondsBoundaryIn() {
		int totalSeconds = Time.getTotalSeconds("00:00:00");
		assertTrue("The time provided does not match the result", totalSeconds == 00);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		fail("The time provided is not valid");
	}

}
