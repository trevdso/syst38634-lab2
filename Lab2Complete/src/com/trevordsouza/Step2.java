package com.trevordsouza;

import java.util.Scanner;

public class Step2 {

	public static void main(String[] args) {
		
		//System.out.println("Vowels in string input: " + getVowelCount(getInputForString()));
		//System.out.println(getCharIndexInString(getInputForString().trim()));
		System.out.println(countUnicodeCodePoints(getInputForString()));
	}
	
	public static String getInputForString() {
		
		String word;
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter your string: ");
		word = input.nextLine();
		
		return word;
	}
	
	public static int getVowelCount(String input) {
		
		int counter = 0;
		
		for(int i = 0; i < input.length(); i++) {
			if(input.charAt(i) == 'a' || input.charAt(i) == 'e' || input.charAt(i) == 'i' 
					|| input.charAt(i) == 'o' || input.charAt(i) == 'u') {
				counter++;
			}
		}
		
		return counter;
	}
	
	public static String getCharIndexInString(String input) {
		
		StringBuilder letters = new StringBuilder();
		StringBuilder digits = new StringBuilder();
		
		for(int i = 0; i < input.length(); i++) {
			if(Character.isAlphabetic(input.charAt(i))) {
				letters.append(input.charAt(i));
			}
			else if(Character.isDigit(input.charAt(i))) {
				digits.append(input.charAt(i));
			}
		}
		
		return "The character at position " + digits.toString()+" is "
			+ letters.toString().charAt(Integer.parseInt(digits.toString()) - 1);
		
	}
	
	public static int countUnicodeCodePoints(String input) {
		
		int counter = 0;
		
		for(int i = 0; i < input.length(); i++) {
			if(Character.isValidCodePoint(input.charAt(i))) {
				counter++;
			}
		}
		
		return counter;
	}

}
