/*
 * Added comments
 */
package com.trevordsouza;

import java.util.Scanner;

public class Step1 {

	public static void main(String[] args) {
		
		//System.out.println("Smallest value in array is: " + smallestVal(getInputForArray()));
		//System.out.println("Average value for array is: " + averageVal(getInputForArray()));
		System.out.println("Middle values for string input: " + middleVals(getInputForString()));
	}
	
	public static int[] getInputForArray() {
		
		int[] array = new int [3];
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the first number: ");
		array[0] = input.nextInt();
		System.out.print("Enter the second number: ");
		array[1] = input.nextInt();
		System.out.print("Enter the third number: ");
		array[2] = input.nextInt();
		
		return array;
	}
	
	public static String getInputForString() {
		
		String word;
		
		Scanner input = new Scanner(System.in);
		System.out.print("Enter your string: ");
		word = input.nextLine();
		
		return word;
	}
	
	public static double smallestVal(int[] array) {
		
		double smallestValue = 0;
		for(int i = 0; i < array.length; i++) {
			if(i == 0) {
				smallestValue = array[i];
			}else if(array[i] < smallestValue) {
				smallestValue = array[i];
			}
		}
		
		return smallestValue;
	}
	
	public static double averageVal(int[] array) {
		
		double average = 0;
		for(int i = 0; i < array.length; i++) {
			average += array[i];
		}
		average = average/array.length;
		
		return average;
	}

	public static String middleVals(String word) {
		
		StringBuilder sb = new StringBuilder();
		if(word.length()%2 == 0) {
			int pre = word.length()/2;
			sb.append(word.charAt(pre-1));
			return sb.append(word.charAt(pre)).toString();
		}else {
			int pre = word.length()/2;
			return sb.append(word.charAt(pre)).toString();
		}
	}
}
